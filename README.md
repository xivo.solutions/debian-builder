# Debian Builder Docker Image

Docker image used to build our debian projects.

## Why a docker image ?

We need to build our debian projects using python2 and python3 regardless of the host distribution.
This container serves two purposes, as it is one of the tools that allows us to :

+ build and test the project as a developper without affecting our environment
+ build and test the project on the ci pipeline

PLEASE note that the use of debian-builder / debuilder is not always needed. It is intended as a tool to reduce the pain of having to install specific dependencies/switch vms/maintain old version of packages of your system that are required to build/run some specific XiVO cases, and yes, it's not that easy to work with. If you can build/test without it, good enough. Just remain aware that the tool is also used on Jenkins at build time.

## Run the image with debuilder

We recommend that you add this function to your .bashrc or .zshrc to be able to launch it:

```sh
function debuilder() {
  local lts=${1:-maia}
  local tag=${2:-latest}

  docker pull xivoxc/debian-builder-${lts}:${tag}
  docker run --rm -it \
    --network host \
    -v /etc/localtime:/etc/localtime:ro \
    -v /etc/timezone:/etc/timezone:ro \
    --user "`id -u`:`id -g`" \
    -v /etc/group:/etc/group:ro \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/shadow:/etc/shadow:ro \
    -v "`pwd`/..":"`pwd`/.." \
    -w "`pwd`" \
    -e DOCKERIZED=true \
    -e DEBFULLNAME="`git config user.name`" \
    -e DEBEMAIL="`git config user.email`" \
    xivoxc/debian-builder-${lts}:${tag}
}
```

You can call it with the lts you want, depending on the xivo version you're working on :

```sh
debuilder luna
```

This bring you inside the container where you can build/test with the right xivo build dependencies.

## Build the image locally without pushing to docker hub

```sh
export TARGET_VERSION=dev-test #any tag you want
chmod +x docker_build.sh
./docker_build.sh
git restore docker_build.sh
```

Then you can run it too :

```sh
debuilder maia dev-test
```

## Build the Image

Run the [debian-builder jenkins job](http://jenkins.xivo.solutions:8080/job/debian-builder/)

There are 3 build mode : dev, rc and prod. Check the created tag on the repos (example for maia: <https://hub.docker.com/r/xivoxc/debian-builder-maia/tags>)

+ with dev, you can build the version you want : it retrieves the ticket id from the commit you pushed it with (like 1234) and tags it to docker hub as `dev-1234`. Then if I did this on maia with TARGET_VERSION 2023.11.00, I could run locally using :

```sh
debuilder maia 2023.11.dev-1234
```

by default latestdev is tagged

```sh
debuilder maia 2023.11.latestdev
```

+ with rc you get a build with latestrc instead of latestdev"
  + latestdev:       2023.11.latestrc
+ with prod, you retag the rc build into three tags :
  + TARGET_VERSION : 2023.11.00
  + version+latest : 2023.11.latest
  + latest:          latest

The last one is used on most our pipelines and shall be used in most cases

## Bump next lts

+ Create a new branch for the current lts with it's major version (`2023.10` for `luna`)
+ in master change docker_build.sh and TARGET_VERSION accordingly. TARGET_VERSION should be set to the first IV and upgraded when specific builds/steps are needed
